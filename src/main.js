import "leaflet/dist/leaflet.css";
import "leaflet-search/dist/leaflet-search.src.css";
import "./main.css";
import L from "leaflet";
import "leaflet-search";

// config map
let config = {
  minZoom: 1,
  maxZoom: 15,
};
// magnification with which the map will start
const zoom = 13;
// co-ordinates Thorndon
const lat = -41.275;
const lng = 174.774;

// calling map
const map = L.map("map", config).setView([lat, lng], zoom);
const mapspastAttribution =
  '<a href="http://mapspast.org.nz">MapsPast</a>, <a href="https://www.library.auckland.ac.nz/databases/record/?record=geodatahub">GeoDataHub</a> and <a href="https://data.linz.govt.nz/">Toitū Te Whenua Land Information New Zealand</a>';

const topo50_2019 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/topo50-2019/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
topo50_2019.addTo(map);
const topo50_2009 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/topo50-2009/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms260_1999 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms260-1999/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms260_1989 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms260-1989/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms1_1979 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms1-1979/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms1_1969 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms1-1969/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms1_1959 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms1-1959/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms15_1949 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com//au.mapspast.org.nz/3857/nzms15-1949/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms13_1939 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com//au.mapspast.org.nz/3857/nzms13-1939/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms13_1929 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms13-1929/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms13_1919 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms13-1919/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms13_1909 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms13-1909/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);
const nzms13_1899 = L.tileLayer(
  "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/3857/nzms13-1899/{z}/{x}/{y}.png",
  {
    attribution: mapspastAttribution,
  }
);

const baseMaps = {
  "Topo50 2019": topo50_2019,
  "Topo50 2009": topo50_2009,
  "NZMS260 1999": nzms260_1999,
  "NZMS260 1989": nzms260_1989,
  "NZMS1 1979": nzms1_1979,
  "NZMS1 1969": nzms1_1969,
  "NZMS1 1959": nzms1_1959,
  "NZMS15 1949": nzms15_1949,
  "NZMS13 1939": nzms13_1939,
  "NZMS13 1929": nzms13_1929,
  "NZMS13 1919": nzms13_1919,
  "NZMS13 1909": nzms13_1909,
  "NZMS13 1899": nzms13_1899,
};
L.control.layers(baseMaps).addTo(map);

function onEachFeature(feature, layer) {
  layer.setIcon(new L.DivIcon());
  layer.bindPopup(feature.properties.html);
}

// adding geojson by fetch
// of course you can use jquery, axios etc.
fetch("tiakigazetteer.geojson")
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    // use geoJSON
    var gjLayer = L.geoJSON(data, {
      attribution:
        '<a href="https://tiaki.natlib.govt.nz/">Tiaki (Alexander Turnbull Library)</a> and the <a href="https://gazetteer.linz.govt.nz/">New Zealand Gazetteer (New Zealand Geographic Board)</a>',
      onEachFeature: onEachFeature,
    }).addTo(map);
    var controlSearch = new L.Control.Search({
      initial: false,
      layer: gjLayer,
      marker: false,
      position: "topleft",
      propertyName: "text",
      zoom: 12,
    });
    map.addControl(controlSearch);
  });
