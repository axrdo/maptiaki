#!/usr/bin/env python3

import argparse
import csv
import fold_to_ascii
import json
import logging
import sys

parser = argparse.ArgumentParser()
parser.add_argument("features", help="a json dictionary of nz place name features from NZGB by feat_id")
parser.add_argument("names", help="a json dictionary of nz place names from NZGB by name_id")
parser.add_argument("places", help="Tiaki place thesuarus entries by irn which have NZGB name_id's")
parser.add_argument("warnings", help="an output csv report")
args = parser.parse_args()

with open(args.features, "r") as j:
    features = json.load(j)
with open(args.names, "r") as g:
    names = json.load(g)
with open(args.places, "r") as p:
    places = json.load(p)

csvfile = open(args.warnings, 'w', newline='')
fieldnames = ["irn", "name_id"]
writer = csv.writer(csvfile)
writer.writerow(fieldnames)

result = {}
for irn in places:
    #print(irn)
    name_id = places[irn]["name_id"]
    #print(irn+"->"+str(name_id))
    if str(name_id) not in names:
        writer.writerow([irn, str(name_id)])
        logging.warning("irn "+irn+" refers to unknown name_id "+str(name_id))
    else:
        feat_id = names[str(name_id)]["feat_id"]
        if str(feat_id) not in result:
            result[str(feat_id)] = features[str(feat_id)]
            result[str(feat_id)]["properties"]["name_id"] ={}
        if str(name_id) not in result[str(feat_id)]["properties"]["name_id"]:
            result[str(feat_id)]["properties"]["name_id"][str(name_id)] = {"name_id": name_id, "name": names[str(name_id)]["name"], "irn":{}}
        result[str(feat_id)]["properties"]["name_id"][str(name_id)]["irn"][str(irn)] = {"irn": irn, "place": places[irn]["place"]}

for feat_id_item in result:
    nn = result[feat_id_item]["properties"]["name_id"]

    for name_id_item in nn:
        for irn_item in nn[name_id_item]["irn"]:
            html = '<a href=\"https://tiaki.natlib.govt.nz/#details=ethesaurus.'+str(nn[name_id_item]["irn"][irn_item]["irn"])+'\" target=\"_blank\">'+nn[name_id_item]["irn"][irn_item]["place"]+'</a>'
            nn[name_id_item]["irn"][irn_item]["html"] = html
        nn[name_id_item]["html"] = "<li>"+"</li><li>".join([nn[name_id_item]["irn"][irn_item]["html"] for irn_item in nn[name_id_item]["irn"]])+"</li>"
    result[feat_id_item]["properties"]["html"] = "<ul>"+"".join([nn[name_id_item]["html"] for name_id_item in nn])+"</ul>"
    
    places = {
        nn[name_id_item]["irn"][irn_item]["place"]
        for name_id_item in nn
        for irn_item in nn[name_id_item]["irn"]
    }
    folds = { 
        fold_to_ascii.fold(p)
        for p in places
    }
    result[feat_id_item]["properties"]["text"] = " ".join(list(places|folds))

csvfile.close()

json.dump({"type": "FeatureCollection", "features": list(result.values())}, sys.stdout, ensure_ascii=False, indent=2)
