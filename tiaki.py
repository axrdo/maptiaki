#!/usr/bin/env python3

import argparse
import json
import logging
from openpyxl import load_workbook
import sys

parser = argparse.ArgumentParser()
parser.add_argument("xslx", help="excel download from the tiaki place thesaurus")
args = parser.parse_args()

index = {}
wb = load_workbook(filename = args.xslx, read_only=True)
ws = wb.active

for row in ws.rows:
    if (row[0].value != "Internal Record Number"):
        assert(str(row[0].value) not in index)
        index[str(row[0].value)]={"irn":row[0].value, "place":row[3].value, "name_id": row[2].value}
    
# Close the workbook after reading
wb.close()

json.dump(index, sys.stdout, ensure_ascii=False, indent=1)
